/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:async';
import 'dart:io';

import 'package:dotenv/dotenv.dart';
import 'package:elrepo_gateway/RetroShare/repositories/timer_repository.dart';
import 'package:elrepo_gateway/RetroShare/repositories/timers_cb.dart';
import 'package:elrepo_gateway/RetroShare/rs_controler.dart';
import 'package:elrepo_gateway/RetroShare/rs_service.dart';
import 'package:elrepo_gateway/api_router.dart';
import 'package:elrepo_gateway/common/providers/storage_provider.dart';
import 'package:elrepo_gateway/utils/config.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as shelf_io;

Future main() async {
  final storageProvider = StorageProvider();
  final rsController = RsController();
  final timersRepository = TimersRepository();
  final rsService = RsService(
      rsController: rsController,
      storageProvider: storageProvider,
      timersRepository: timersRepository);
  await rsService.init();

  // Add promiscuity timer
  if(AppConfig.activatePromiscuity){
    timersRepository.addPeriodic(PROMISQUITY_KEY, promisquityCheckCb, AppConfig.promiscuityCheckTime);
  }

  // Automatic forum subscription
  if(AppConfig.activateSubscribeRepoForums) {
    timersRepository.addPeriodic(AUTOMATIC_SUBSCRIPTION_KEY, subscribeRepoForums, AppConfig.subscribeRepoForumsCheckTime);
  }

  // Befriending tiers
  if(AppConfig.befriendingTiers) {
    rsService.befriendTiers();
  }

  final port = AppConfig.appPort;
  final apiRouter = ApiRouter().router;
  final handler = const Pipeline().addMiddleware(logRequests()).addHandler(apiRouter); // Configure a pipeline that logs requests.

  final server = await shelf_io.serve(handler, InternetAddress.anyIPv4, port);
  print('Server listening at http://${server.address.host}:${server.port}');
  print("Use Ctrl-C (SIGINT) to stop running the application.");

  await terminateRequestFuture();
  await server.close();
}


/// Returns a [Future] that completes when the process receives a
/// [ProcessSignal] requesting a shutdown.
///
/// [ProcessSignal.sigint] is listened to on all platforms.
///
/// [ProcessSignal.sigterm] is listened to on all platforms except Windows.
Future<void> terminateRequestFuture() {
  final completer = Completer<bool>.sync();

  // sigIntSub is copied below to avoid a race condition - ignoring this lint
  // ignore: cancel_subscriptions
  StreamSubscription? sigIntSub, sigTermSub;

  Future<void> signalHandler(ProcessSignal signal) async {
    print('Received signal $signal - closing');

    final subCopy = sigIntSub;
    if (subCopy != null) {
      sigIntSub = null;
      await subCopy.cancel();
      sigIntSub = null;
      if (sigTermSub != null) {
        await sigTermSub!.cancel();
        sigTermSub = null;
      }
      completer.complete(true);
    }
  }

  sigIntSub = ProcessSignal.sigint.watch().listen(signalHandler);

  // SIGTERM is not supported on Windows. Attempting to register a SIGTERM
  // handler raises an exception.
  if (!Platform.isWindows) {
    sigTermSub = ProcessSignal.sigterm.watch().listen(signalHandler);
  }

  return completer.future;
}

