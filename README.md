# elrepo_gateway

Pure Dart web gateway for [elrepo.io](https://elrepo.io) nodes. 

It uses [Shelf](https://pub.dev/packages/shelf) package to create a server that interacts with 
[RetroShare service](https://build.opensuse.org/package/binaries/network:retroshare/retroshare-service-unstable/AppImage). 

## Run

1. Clone this repo, and install deps:

```bash
pub get
```

2. Configure environment variables as you wish:

```bash
cp env.example .env
```

3. Run `retroshare-service`. The server will create a new location if not exists already or will try to log in using the 
content on the auth file configured on the environment. 