/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:shelf/shelf.dart';
import 'package:shelf_router/shelf_router.dart';
import 'package:shelf_static/shelf_static.dart';

import 'files/files_controller.dart';
import 'forums/forums_controller.dart';
import 'remote_controller/rsremote_controller.dart';


class ApiRouter {

  Handler get router {

    final forums = ForumsController();
    final forumsRouter = Router()
      ..get('/forums', forums.getAllForums)
      ..get('/forums/<forumId>', forums.getPostDetail);

    final rsRemote = RsRemoteController();
    final remoteRouter = Router()
      ..get('/rsremote', rsRemote.imRemoteController)
      ..get('/rsremote/<newUser>', rsRemote.createNewToken)
      ..post('/rsremote/<rsClass>/<rsMethod>', rsRemote.rsReverseProxy);

    final staticRouter = Router()
      ..mount('/webfonts/', createStaticHandler('./assets/webfonts'))
      ..mount('/css/', createStaticHandler('./assets/css'))
      ..mount('/img/', createStaticHandler('./assets/img'));

    final rsFilesController = RsFilesController();
    final filesRouter = Router()
      ..get('/rsfiles/<hash>/<filename>', rsFilesController.getFileByHash);

    const prefix = '/';
    final handler = Router()
      ..mount(prefix, staticRouter)
      ..mount(prefix, forumsRouter)
      ..mount(prefix, remoteRouter)
      ..mount(prefix, filesRouter);

    return const Pipeline()
    // .addMiddleware(corsHeaders())
    //     .addMiddleware(jsonContentTypeResponse())
        .addHandler(handler);
  }
}
