
class Auth {
  String identityId ;
  String locationId ;
  String passphrase;
  String apiUser;

  Auth({
    required this.identityId,
    required this.locationId,
    required this.passphrase ,
    required this.apiUser,
  });

  /// Used when getting auth from JSON object
  Auth.fromJson(Map<dynamic,dynamic> location)
      : locationId = location["locationId"],
        identityId = location["identityId"],
        passphrase = location["passphrase"],
        apiUser = location["apiUser"];

  Map<String, dynamic> toJson() => {
    'identityId': identityId,
    'locationId': locationId,
    'passphrase': passphrase,
    'apiUser': apiUser,
  };



}