import 'dart:convert';
import 'dart:io';

import 'package:elrepo_gateway/RetroShare/models/auth.dart';
import 'package:elrepo_gateway/utils/config.dart';
import 'package:elrepo_gateway/utils/utils.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:retroshare_dart_wrapper/rs_models.dart' as rsModels;
import 'package:elrepo_lib/repo.dart' as repo;

class RsController {

  /// Create a random name location using mac address to get the name
  ///
  /// Stores the new location and identity into a file defined on config
  /// `authFile` path
  Future<Auth> createLocation() async {
    print("createLocation - Trying to create random name");
    var randomName = await getRandomName();
    print("createLocation - Random name created $randomName");
    if (randomName.isEmpty) throw("Can't create random name");
    final passphrase = getRandomString(10);
    print("createLocation - Creating new location");
    final newLocation = await repo.signUp(passphrase, randomName, api_user: randomName);
    print("createLocation - Logging in to create an identity");
    await repo.login(passphrase, newLocation);
    return Auth.fromJson({
      "locationId": newLocation.rsPeerId,
      "passphrase": passphrase,
      "apiUser": randomName,
      "identityId": await rs.RsIdentity.getOrCreateIdentity()
    });
  }

  Future<int> login(Auth auth) async {
    rs.rsLocalState.authApiUser = auth.apiUser;
    return await repo.login(
        auth.passphrase,
        rsModels.Location.fromJson({
          "mLocationName" : auth.apiUser,
          "mLocationId" : auth.locationId
        })
    );
  }
}