import 'dart:async';
import 'dart:io';

import 'package:elrepo_gateway/RetroShare/models/auth.dart';
import 'package:elrepo_gateway/RetroShare/repositories/timer_repository.dart';
import 'package:elrepo_gateway/RetroShare/rs_controler.dart';
import 'package:elrepo_gateway/common/providers/storage_provider.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;

class RsService {

  RsService({
    required this.rsController,
    required this.storageProvider,
    required this.timersRepository,
  });

  final RsController rsController;
  final StorageProvider storageProvider;
  final TimersRepository timersRepository;

  /// Initialize services in this method.
  ///
  /// Implement this method to initialize services, read values from [options]
  /// and any other initialization required before constructing [entryPoint].
  ///
  /// This method is invoked prior to [entryPoint] being accessed.
  Future<void> init() async {
    // Check if retroshare is running and logged in, if not, throw an exception
    for(var x = 0; !await rs.rsClient.isRetroshareRunning(); x++) {
      print("RS service is down");
      await Future.delayed(const Duration(seconds: 5), () => "1");
      // Stop working after 30 second of trying to start
      if(x*5 >= 30) {
        exit(1);
      }
    }

    final auth = await getOrCreateDefaultLocation();

    while(!(await rs.RsLoginHelper.isLoggedIn())) {
      print("RS is not logged in");
      print("Trying to log in");
      await rsController.login(auth);
      await Future.delayed(const Duration(seconds: 5), () => "1"); // Aqueduct will stop working after 30 second of trying to start
    }
  }

  Future<Auth> getOrCreateDefaultLocation() async {
    Auth auth;
    // If no location is created created one randomly
    if (await rs.RsLoginHelper.getDefaultLocation() == null) {
      print("No retroshare locations found, trying to create one");
      auth = await rsController.createLocation();
      storageProvider.storeAuth(auth);
    } else {
      auth = await storageProvider.getAuth();
    }
    return auth;
  }

  void befriendTiers() {
    repo.befriendingTiers().then(
            (value) => value
                ? print('Befriending success')
                : print('Error, befriending failure'));
  }
}