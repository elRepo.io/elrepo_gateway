import 'dart:async';

class TimersRepository {
  Map<String, Timer> _timers = <String, Timer>{};

  Map<String, Timer> get timers => _timers;

  void addPeriodic(String key, cb(Timer t) , Duration duration) =>
      _timers = {..._timers, ...{key : Timer.periodic(duration, cb)} };

  void stopTimer(String key) => _timers[key]?.cancel();

  Timer? getTimer(String key) => _timers[key];
}