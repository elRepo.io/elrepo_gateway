import 'package:elrepo_lib/repo.dart' as repo;
import 'dart:async';

// Timers keys
const PROMISQUITY_KEY = "promisquityCheckKey";
const AUTOMATIC_SUBSCRIPTION_KEY = "subscribeRepoForumsKey";

// Timers
void promisquityCheckCb(Timer timer) {
  repo.doPromiscuity();
}

void subscribeRepoForums(Timer timer) {
  repo.subscribeToRepoForums();
}