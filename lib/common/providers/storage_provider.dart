

import 'dart:convert';
import 'dart:io';

import 'package:elrepo_gateway/RetroShare/models/auth.dart';
import 'package:elrepo_gateway/utils/config.dart';

class StorageProvider {

  /// Store an auth on the path defined on the auth config
  void storeAuth(Auth auth) {
    File(AppConfig.authFile)
        .writeAsStringSync(jsonEncode(auth), mode: FileMode.write);
  }

  Future<Auth> getAuth() async {
    print("Reading ${AppConfig.authFile}");
    final jsonAuth = await File(AppConfig.authFile)
        .readAsString()
        .then((fileContents) => json.decode(fileContents));
    return Auth.fromJson(jsonAuth);
  }
}