'''
/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
'''




import random, uuid

# List extracted from https://gitlab.com/-/snippets/2088482
wordlist = """alcachofa
berenjena
espárrago
frijoles
alubias
remolacha
brócoli
col
repollo
zanahoria
coliflor
apio
maíz
calabacín
pepino
berenjena
ajo
puerro
lentejas
lechuga
champiñón
cebolla
guisantes
pimiento
pepino
papa
patata
calabaza
rabanito
arroz
centeno
espinaca
calabacita
batata
tomate
nabo
berro
trigo
almendra
manzana
albaricoque
aguacate
banana
mora
arándano
cereza
castaña
coco
dátil
higo
uva
pomelo
avellana
limón
lima
mango
melón
guinda
nectarina
naranja
papaya
durazno
cacahuete
pera
piña
ananá
ciruela
frambuesa
fresa
mandarina
sandía
plátano
toronja
frutilla
melocotón
maní
""".split()

mac_addres = (''.join(['{:02x}'.format((uuid.getnode() >> ele) & 0xff)
for ele in range(0,8*6,8)][::-1]))

random.seed(a=mac_addres, version=2)
print(
	wordlist[random.randrange(wordlist.__len__())] +
	mac_addres[6:] # Get last 6 digits of mac (serial num)
)