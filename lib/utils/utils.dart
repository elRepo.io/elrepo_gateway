/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:elrepo_lib/models.dart';

import 'config.dart';

String getRandomString(int length) {
  const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  var _rnd = Random();
  return String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
}

// Return yyyy-mm-dd at hh:mm
String getFormatedTime(int time) {
  DateTime date = getTimeSinceEpoch(time);
  return date.year.toString() + "-"
      + date.month.toString().padLeft(2, '0') + "-"
      + date.day.toString().padLeft(2, '0') + " at "
      + date.hour.toString().padLeft(2, '0') + ":"
      + date.minute.toString().padLeft(2, '0');
}
DateTime getTimeSinceEpoch(int time) =>
    DateTime.fromMillisecondsSinceEpoch(time * 1000);

class ContentTypeIcon {
  // For each contentype returns a font awesome icon class
  static final Map<String, String> contentTypeIcons = {
    ContentTypes.ref : "fa-link",
    ContentTypes.image : "fa-image",
    ContentTypes.audio : "fa-music",
    ContentTypes.video : "fa-video",
    ContentTypes.document : "fa-book",
    ContentTypes.text : "fa-paragraph",
    ContentTypes.file : "fa-file"
  };

  static String? getContentTypeIcon(String contentType) {
    if (contentTypeIcons.containsKey(contentType)) {
      return contentTypeIcons[contentType];
    } else {
      // the default icon is the file icon for unknown types
      return contentTypeIcons[ContentTypes.file];
    }
  }
}


/// White list support:
/// rsClass : [rsMethod]
/// rsClass : ["*"]
/// ["*"] : ["*"]
/// See test for use case
bool whiteListReverseProxyValidator(String rsClass, String rsMethod, {
  Map<String, List<String>> whiteList = REVERSEPROXY_WHITELIST
}) =>
    (whiteList.containsKey("*") || whiteList.containsKey(rsClass))
        || (whiteList.containsKey(rsClass) &&
        (whiteList[rsClass]!.contains("*") ||  whiteList[rsClass]!.contains(rsMethod)));


/// Call a python script that return a random name based on mac
Future<String> getRandomName() async {
  String scriptPath = "lib/utils/random_name.py";
  String venvUrl = '/usr/bin/python3.8';

  // Run python script with args
  String result = "";
  await Process.run(venvUrl, [scriptPath]).then((value) {
    result = value.stdout;
  });
  return result.replaceAll("\n", "");
}
