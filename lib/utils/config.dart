/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:io';

import 'package:dotenv/dotenv.dart';

class AppConfig {
  static var dotEnv = DotEnv(includePlatformEnvironment: false)..load(); // includePlatformEnvironment not override with system env variables
  static Map<String, String> systemEnv = Platform.environment;

  static bool asBool(String s) => s.toLowerCase() == 'true';


  // Promiscuity
  static bool activatePromiscuity = asBool(systemEnv['PORMISQUITY_ACTIVATE'] ?? dotEnv['PORMISQUITY_ACTIVATE'] ?? 'true');

  static Duration promiscuityCheckTime = Duration(
      seconds: int.parse(systemEnv['PORMISQUITY_TIME'] ?? dotEnv['PORMISQUITY_TIME'] ?? "3"));

  // Autosubscribe
  static bool activateSubscribeRepoForums = asBool(systemEnv['AUTOSUBSCRIBE_ACTIVATE'] ?? dotEnv['AUTOSUBSCRIBE_ACTIVATE'] ?? 'false');
  static Duration subscribeRepoForumsCheckTime = Duration(
      seconds: int.parse(systemEnv['AUTOSUBSCRIBE_TIME'] ?? dotEnv['AUTOSUBSCRIBE_TIME'] ?? "3"));

  // App config
  static int appPort = int.parse(systemEnv['APP_PORT'] ?? dotEnv['APP_PORT'] ?? "8888");
  static String authFile = systemEnv['AUTH_FILE'] ?? dotEnv['AUTH_FILE'] ?? "auth.json";
  static bool befriendingTiers = asBool(systemEnv['BEFRIENDING'] ?? dotEnv['BEFRIENDING'] ?? 'true');
  static bool enableRemoteControl = asBool(systemEnv['REMOTE_CONTROL'] ?? dotEnv['REMOTE_CONTROL'] ?? 'true');
}

const REVERSEPROXY_WHITELIST =
  {
    // Accepts * to set all
    //    "*" : ["*"],
    "rsGxsForums" : [
      "*"
    ],
    "rsFiles" : [
      "*"
    ],
    "rsGxsChannels" : [
      "createCommentV2",
      "getChannelContent",
      "getChannelsSummaries",
      "getContentSummaries",
      "subscribeToChannel"
    ],
    "rsGxsCircles" : [
      "getCircleDetails",
      "requestCircleMembership"
    ],
    "rsMsgs" : [
      "MessageDelete",
      "getMessage",
      "getMessageSummaries",
      "sendMail",
    ],
    "rsPeers" : [
      "isFriend",
      "getGroupInfoList",
      "getFriendList",
      "getOnlineList",
      "getPeerDetails",
      "parseShortInvite",
      "connectAttempt",
      "addSslOnlyFriend",
      "acceptInvite",
      "GetRetroshareInvite",
    ],
    "rsIdentity": [
      "getIdDetails",
      "requestIdentity",
      "isKnownId",
      "getOwnPseudonimousIds",
      "createIdentity",
    ],
    "rsLoginHelper": [
      "getLocations",
      "isLoggedIn",
      "attemptLogin"
    ],
    "rsBroadcastDiscovery" : ["getDiscoveredPeers"]
  };
