/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */


import 'dart:convert';

import 'package:elrepo_gateway/utils/config.dart';
import 'package:elrepo_gateway/utils/utils.dart';
import 'package:elrepo_lib/models.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:shelf/shelf.dart';

// https://github.com/dart-archive/shelf_proxy/blob/master/lib/shelf_proxy.dart
class RsRemoteController {

  Future<Response> imRemoteController(Request request) async =>
    AppConfig.enableRemoteControl
        ? Response.ok(jsonEncode({'retval' : true}))
        : Response.ok(jsonEncode({'retval': false}));

  Future<Response> createNewToken(Request request, String newUser) async {
    if (!AppConfig.enableRemoteControl) {
      return Response.ok({"retval" : false});
    }

    final String password = getRandomString(10);

    var res = await rs.RsJsonApi.authorizeUser(newUser, password);

    if (res) {
      var auth = GatewayAuthObject(
          newUser,
          password,
          rs.rsLocalState.authIdentityId!
      ).toJson()..['bearer'] = rs.rsClient.makeAuthHeader(newUser, password);
      return Response.ok(
          jsonEncode(auth));
    }
    return Response.internalServerError();
  }

  /// Proxy a [rsClass]/[rsMethod] to retroshare
  ///
  /// This class and methos have to be behinf the [whiteListReverseProxyValidator]
  ///
  /// It requires a Basic auth token for the previous authorized user, using
  /// [createNewToken] function. Te token basically is a basic auth string on
  /// the authorization header. This token is returned when [createNewToken] is
  /// called and basically is generated using `rs.rsClient.makeAuthHeader` (just
  /// a simple operation doing `base64Encode(utf8.encode('$username:$password'))`)
  ///
  /// The body is also proxied and has to be json encodable.
  ///
  /// When calling to `rsFiles/alreadyHaveFile` it "translate" the files url to
  /// the `thisServerUrl/rsfiles` endpoint.
  Future<Response> rsReverseProxy(Request request, String rsClass, String rsMethod) async {
    if (!AppConfig.enableRemoteControl) {
      return Response.ok({"retval" : false});
    } else if (!whiteListReverseProxyValidator(rsClass, rsMethod)) {
      return Response.unauthorized(
          jsonEncode({"retval": false, "message": "Method not allowed"}));
    } else if (!request.headers.containsKey("authorization")) {
      return Response.unauthorized(
          jsonEncode({"retval": false, "message": "Bearer token not provided"}));
    }

    final String path = "/$rsClass/$rsMethod";
    final String auth = request.headers["authorization"]!;
    final String body = await request.readAsString();
    final Map<String, dynamic> decodedJson = body.isNotEmpty
        ? jsonDecode(body) : {};

    print("Reverse proxy body for $path: $body");

    try {
      final res = await rs.rsClient.apiCall(path, params: decodedJson, basicAuth: auth);

      // Modify the file path to return server rsfiles endpoint
      if (rsClass == 'rsFiles' && rsMethod == "alreadyHaveFile" && res["retval"] == true) {
        res["info"]["path"] =
            "http://${request.url.host}/rsfiles/"
              + "${decodedJson["hash"]}/"
                + res["info"]["path"].split('/').last;
      }
      return Response.ok(jsonEncode(res));
    } catch (err) {
      print("Error calling RS");
      rethrow;
    }
  }
}
