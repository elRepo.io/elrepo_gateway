/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'dart:io';

import 'package:elrepo_gateway/common/exceptions/not_found_exception.dart';
import 'package:elrepo_lib/models.dart';
import 'package:mustache_template/mustache_template.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:elrepo_gateway/utils/repo_gateway_service.dart';
import 'package:elrepo_gateway/utils/utils.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_router/shelf_router.dart';
import 'package:elrepo_lib/models.dart';

import 'lib/html_utils.dart';

class ForumsController {

  Future<String> _addIntoIndexHTML(String body,) async {
    var ownId = await GatewayService.getOwnIdentityDetails();
    var avatar = HtmlUtils.getHtmlAvatar(ownId["mAvatar"]["mData"]["base64"]);

    return Template(
        await File(Directory.current.path + '/assets/templates/index.html')
            .readAsString(), name: 'index.html')
        .renderString(
        {
          'body': body,
          'identity': ownId,
          'headerAvatar': avatar
        }
    );
  }

  Future<Response> getAllForums(Request request) async {
    var parsed = [];
    (await repo.exploreContent())
        .forEach((element) {
      final e = {}
        ..["parsedTags"] = HtmlUtils.getTagsFromString(element.mMsgName)
        ..["groupId"] = element.mGroupId
        ..["msgId"] = element.mMsgId
        ..["title"] = element.postMetadata.title
        ..["summary"] = element.postMetadata.summary
        ..["iconType"] = ContentTypeIcon.getContentTypeIcon(
            element.postMetadata.contentType)
        ..["time"] = getFormatedTime(element.mPublishTs.xint64);
      parsed.add(e);
    });
    Template template = Template(
        await File(Directory.current.path + '/assets/templates/forums.html')
            .readAsString(),
        name: 'forums.html');
    return Response.ok(
        await _addIntoIndexHTML(template.renderString({'posts': parsed})),
        headers: {HttpHeaders.contentTypeHeader: ContentType.html.toString()});
    // ..contentType = ContentType.html;
  }

  // @Operation.get('forumId')
  Future<Response> getPostDetail(Request request) async {
    final msgIds = request.requestedUri.queryParameters['msgId'];
    final forumId = request.params['forumId'] as String;

    if (msgIds == null) {
      throw AssertionError("Query parameter msgId not found");
    }

    // Get publication data
    final PostDetail? rsResponse;
    try {
      rsResponse = await repo.getPostDetails(forumId, msgIds);
    } catch (e){
      throw NotFoundException(message: "Content not found");
    }

    final comments =  await repo.getForumComments(forumId, msgIds);
    await Future.forEach(comments, (Map<String, dynamic> element) async {
      var author = (await rs.RsIdentity.getIdentitiesInfo([element["mMeta"]["mAuthorId"]]))[0];
      element["mMsg"] = jsonDecode(element["mMsg"]);
      element["mMsgTime"] =
          getFormatedTime(element["mMeta"]["mPublishTs"]["xint64"]);
      element["mNickname"] = author.name.isNotEmpty ? author.name : author.mId;
      element ["avatar"] = HtmlUtils.getHtmlAvatar( author.avatar);
          // element["authorMesageData"]["mAvatar"]["mData"]["base64"]);
    });

    Template template = Template(
        await File(
            Directory.current.path + '/assets/templates/publication.html')
            .readAsString(),
        name: 'publication.html');

    final payloadLinks = <Map<String, dynamic>>[];
    if (rsResponse.postBody?.payloadLinks.isNotEmpty ?? false){
      for(var payload in rsResponse.postBody!.payloadLinks) {
        payloadLinks.add({
          "hash": payload.hash,
          "filename": payload.filename,
        });
      }
    }

    var body =
        template.renderString({
          "iconType": ContentTypeIcon.getContentTypeIcon(rsResponse.postMetadata!.contentType),
          "title": rsResponse.postMetadata?.title,
          "text": HtmlUtils.jsonToHtmlPrettyPrint(rsResponse.postBody?.text ?? "", blackList: ["tags"]),
          "parsedTags": HtmlUtils.getTagsFromString(rsResponse.postDetails!.mMsg),
          "payloadLinks": payloadLinks,
          "contentType": rsResponse.postMetadata?.contentType,
          "comments": comments,
          "authorNickname": rsResponse.authorId,
          "publishTs": getFormatedTime( rsResponse.rsMetadata!.mPublishTs.xint64)
        });
    return Response.ok(
        await _addIntoIndexHTML(body),
        headers: {HttpHeaders.contentTypeHeader: ContentType.html.toString()}
    );
  }
}
