import 'dart:convert';
import 'package:elrepo_lib/repo.dart' as repo;

/// This class contain a bunch of static functions used to prepare RS data to print
/// it on moustache templates
class HtmlUtils {

  // See jsonToHtmlPrettyPrint
  static String _recursiveOrNot(dynamic element, tabulation) =>
      element is Map || element is List ?
      "<br>" + _recursiveJson(element, tabulation: tabulation + "&emsp;")
          : element.toString() + "<br>";

  // See jsonToHtmlPrettyPrint
  static String _recursiveJson(dynamic jsonMap, {String tabulation = ""}) {
    String htmlString = "";
    if (jsonMap is Map) {
      for (final key in jsonMap.keys) {
        htmlString += tabulation + "<strong>" + key + " : </strong>";
        htmlString += _recursiveOrNot(jsonMap[key], tabulation);
      }
    }
    else if (jsonMap is List) {
      for (final element in jsonMap) {
        htmlString += _recursiveOrNot(element, tabulation);
      }
    }
    return htmlString;
  }

  // Get a Json and print key value in a html string
  static String jsonToHtmlPrettyPrint(String jsonString, {List<String> blackList = const []}) {
    try {
      final Map<String, dynamic> jsonMap = jsonDecode(jsonString);
      if (blackList.isNotEmpty)
        jsonMap.removeWhere((key, value) => blackList.contains(key));
      return _recursiveJson(jsonMap);
    } on Exception {
      return jsonString;
    }
  }

  // It return a list to use moustache inverted sections (that only work with lists)
  static List<dynamic>? getHtmlAvatar(String b64String) =>
      b64String.isEmpty ? [] : [
        {"data": b64String}
      ];

  // Get #tags list from a string
  static List<dynamic> getTagsFromString(String mMsg) =>
      json
          .decode(mMsg)["tags"]
          ?.map((hashtag) => {
        "name": hashtag.contains("#") ?  hashtag : '#' + hashtag,
        "hexColor": "#" + stringToHexColor(hashtag)
      })
          ?.toList()
          ?? repo.findHashTags(mMsg).map((hashtag) => {
        "name": "#" + hashtag,
        "hexColor": "#" + stringToHexColor(hashtag)
      }).toList();

  // https://stackoverflow.com/questions/3426404/create-a-hexadecimal-colour-based-on-a-string-with-javascript
  static String stringToHexColor(str) { // java String#hashCode
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
      hash = str.codeUnitAt(i) + ((hash << 5) - hash);
    }
    var c = (hash & 0x00FFFFFF)
        .toRadixString(16)
        .toUpperCase();
    return "00000".substring(0, 6 - c.length) + c;
  }
}