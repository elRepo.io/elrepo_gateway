/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:io';
import 'package:mime/mime.dart';
import 'package:elrepo_lib/repo.dart';
import 'package:shelf/shelf.dart';

class RsFilesController {

  Future<Response> getFileByHash(Request request, String hash, String filename) async {
    String? path = await getPathIfExists(hash);
    if (path == null || path.isEmpty) {
      return Response.notFound("");
    }
    return Response.ok(File(path).openRead(),
        headers: {HttpHeaders.contentTypeHeader: ContentType.binary.toString()});
    // ..contentType = new ContentType("application","octet-stream");
  }
}
