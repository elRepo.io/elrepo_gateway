/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elrepo_gateway/utils/config.dart';
import 'package:elrepo_gateway/utils/repo_gateway_service.dart';
import 'package:elrepo_gateway/utils/utils.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elrepo_lib/retroshare.dart' as rs;

import 'harness/app.dart';


Future main() async {
  final harness = Harness()..install();

  test("GET /example returns 200 {'key': 'value'}", () async {
    expectResponse(await harness.agent.get("/example"), 200, body: {"key": "value"});
  });

  test("files", () async {
    String path = await repo.getPathIfExists("d689b3b9bb417c445e3a3b75c4aed46921ba66d0");
    print(path);
  });

  test("randomHex", () async {
    String hex = stringToHexColor("adfafnksgdnksdanksdf");
    print(hex);
  });

  test("testGetSyncData", () async {
    rs.initRetroshare(
        identityId: AUTH.identityId,
        locationId: AUTH.locationId,
        passphrase: AUTH.passphrase,
        apiUser: AUTH.apiUser
    );
//    var res = await GatewayService.getSyncedContentMetadata();
//    res.forEach((element) {
//      var list =  repo.findHashTags(element["mMsg"]);
//      element["parsedTags"] = list.map((hashtag) => {"name" : "#"+hashtag}).toList();
//    });
//    print(res);
  });

  test("getOwnIdDetails", () async {
    rs.initRetroshare(
        identityId: AUTH.identityId,
        locationId: AUTH.locationId,
        passphrase: AUTH.passphrase,
        apiUser: AUTH.apiUser
    );
    var ownId= await GatewayService.getOwnIdentityDetails();
    print(ownId);
//    print("---------");
//    ownId = GatewayService.getCacheDetails();
//    print(ownId['mNickname']);
  });


  test("authorizeToken", () async {
    rs.initRetroshare(
        passphrase: AUTH.passphrase,
        apiUser: AUTH.apiUser
    );

    // Here the POST api call to aqueductIp:8888/rsremote/:newUser
    var res = await harness.agent.post("/rsremote/userTest");
    // The response is a username and a new random password associated
    print(res);
  });

  test("reverseProxy", () async {

    // Key given by use "/rsremote/:userName"
    String apiUser = "userTest", key  = "FFb938Lykp";

    // This lines are a copy of rsApiCall on RetroShare.dart
    String baseUrl = "http://localhost:8888/rsremote";
    String path  = "/rsGxsForums/getForumsSummaries";
//    String path = "/rsJsonApi/getAuthorizedTokens";
    final basicAuth = rs.makeAuthHeader(apiUser, key);
    Map<String, dynamic> params = {
      'forumId': "479328f21c718e6307d5bfbe0d8ac8a0"
    };

    rs.setRetroshareServicePrefix(baseUrl);
    final res = await rs.rsApiCall(
      path,
//        params: params,
      basicAuth: basicAuth,
    );

    print(res);
  });

  test("whiteListReverseProxy", () async {
    String rsClass = "rsGxsForums", rsMethod = "getForumsSummaries";
    var whitelist = {
      "rsGxsForums" : [
        "getForumsSummaries"
      ],
    };
    print(whiteListReverseProxyValidator(rsClass, rsMethod, whiteList: whitelist)); //true

    rsClass = "rsJsonApi"; rsMethod = "getAuthorizedTokens";
    print(whiteListReverseProxyValidator(rsClass, rsMethod, whiteList: whitelist)); //false

    whitelist = { };
    print(whiteListReverseProxyValidator(rsClass, rsMethod, whiteList: whitelist)); //false

    whitelist = { "rsJsonApi" : ["*"]};
    print(whiteListReverseProxyValidator(rsClass, rsMethod, whiteList: whitelist)); //true

    whitelist = { "*" : ["*"]};
    print(whiteListReverseProxyValidator(rsClass, rsMethod, whiteList: whitelist)); //true

    rsClass = "rsGxsForums"; rsMethod = "getForumsSummaries";
    whitelist = { "rsJsonApi" : ["*"]};
    print(whiteListReverseProxyValidator(rsClass, rsMethod, whiteList: whitelist)); //false

  });

  test("testRsRemoteFiles", () async {
    rs.initRetroshare(
        identityId: AUTH.identityId,
        locationId: AUTH.locationId,
        passphrase: AUTH.passphrase,
        apiUser: AUTH.apiUser
    );
    await harness.agent.post("/rsremote/rsFiles/alreadyHaveFile",
      body: {"hash": "e9c1e397d8dcf55829929a887dd26a382a35180d"});

  });

  test("getRandomName", () async {


  });
}
